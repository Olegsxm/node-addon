// hello.cc
#include <node.h>
#include "rabbit.h"
#include <iostream>

namespace platoon {
  using namespace std;

  using v8::FunctionCallbackInfo;
  using v8::Isolate;
  using v8::Local;
  using v8::NewStringType;
  using v8::Object;
  using v8::String;
  using v8::Value;

  void EncryptMessage(const FunctionCallbackInfo<Value>& args) {
      Isolate* isolate = args.GetIsolate();
      RabbitCipher rabbit;

      v8::String::Utf8Value _message(args[0]);
      v8::String::Utf8Value _key(args[1]);

      std::string msg(*_message);
      std::string key(*_key);

      std::string enc = rabbit.encryptMessage(msg, key);

      Local<String> retval = String::NewFromUtf8(isolate, enc.c_str());
      args.GetReturnValue().Set(retval);
  }

  void DecryptMessage(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
      RabbitCipher rabbit;

      v8::String::Utf8Value _message(args[0]);
      v8::String::Utf8Value _key(args[1]);

      std::string msg(*_message);
      std::string key(*_key);

      std::string enc = rabbit.decryptMessage(msg, key);

      Local<String> retval = String::NewFromUtf8(isolate, enc.c_str());
      args.GetReturnValue().Set(retval);
  }

  void Initialize(Local<Object> exports)
  {
    NODE_SET_METHOD(exports, "encrypt", EncryptMessage);
    NODE_SET_METHOD(exports, "decrypt", DecryptMessage);
  }

  NODE_MODULE(addon, Initialize)
} // namespace platoon