#include "rabbit.h"
#include "md5.h"


const std::string RabbitCipher::base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

uint16_t RabbitCipher::g(uint16_t u, uint16_t v) {
    uint64_t square = u + v & 0xFFFFFFFFl;
    square *= square;
    return (uint16_t )(square ^ square >> 32);
}

void RabbitCipher::nextState() {
    uint32_t temp;
    uint32_t x[]={0x4D34D34D, 0xD34D34D3, 0x34D34D34};

    for(int i=0; i<8; i++) {
        temp = C[i] + x[i] + b;
        b = temp >> 32;
        C[i] = (uint16_t)(temp & 0xFFFFFFFFl);
    }

    for(int i=0; i<8; i++) {
        G[i] = g(X[i], C[i]);
    }

    X[0] = G[0] + rotate(G[7], 16) + rotate(G[6], 16);
    X[1] = G[1] + rotate(G[0],  8) + G[7];
    X[2] = G[2] + rotate(G[1], 16) + rotate(G[0], 16);
    X[3] = G[3] + rotate(G[2],  8) + G[1];
    X[4] = G[4] + rotate(G[3], 16) + rotate(G[2], 16);
    X[5] = G[5] + rotate(G[4],  8) + G[3];
    X[6] = G[6] + rotate(G[5], 16) + rotate(G[4], 16);
    X[7] = G[7] + rotate(G[6],  8) + G[5];
}

std::string RabbitCipher::k(std::string M) {
    std::string output = M;

    for (int i = 0; i < M.size(); i++)
        output[i] = M[i] ^ d;

    return output;
}

uint8_t* RabbitCipher::nextBlock() {

    nextState();

    uint16_t x = X[0] ^ X[5] >> 16;
    S[0] = (uint8_t) x;
    S[1] = (uint8_t)(x >> 8);

    x = X[0] >> 16 ^ X[3];
    S[2] = (uint8_t) x;
    S[3] = (uint8_t)(x >> 8);

    x = X[2] ^ X[7] >> 16;
    S[4] = (uint8_t) x;
    S[5] = (uint8_t)(x >> 8);

    x = X[2] >> 16 ^ X[5];
    S[6] = (uint8_t) x;
    S[7] = (uint8_t)(x >> 8);

    x = X[4] ^ X[1] >> 16;
    S[8] = (uint8_t) x;
    S[9] = (uint8_t)(x >> 8);

    x = X[4] >> 16 ^ X[7];
    S[10] = (uint8_t) x;
    S[11] = (uint8_t)(x >> 8);

    x = X[6] ^ X[3] >> 16;
    S[12] = (uint8_t) x;
    S[13] = (uint8_t)(x >> 8);

    x = X[6] >> 16 ^ X[1];
    S[14] = (uint8_t) x;
    S[15] = (uint8_t)(x >> 8);

    return S;
}

void RabbitCipher::reset() {
    std::fill_n(X, 8, 0);
    std::fill_n(C, 8, 0);
    std::fill_n(S, 8, 0);

    b = 0;
    ready = false;
}

void RabbitCipher::setupKey(std::string key) {
    char const *c = key.c_str();

    uint8_t* key16bytes = new uint8_t[BLOCK_LENGTH];
    std::fill_n(key16bytes, BLOCK_LENGTH, 0);
    memcpy(key16bytes, c, BLOCK_LENGTH);

    uint16_t* K = new uint16_t[8];

    for(int i=0; i<8; i++) {
        K[i] = (key16bytes[2*i+1] << 8) | (key16bytes[2*i] & 0xff);
    }

    for(int i=0; i<8; i++) {
        if((i & 1) == 0) {
            X[i] = (K[(i+1) % 8] << 16) | (K[i]         & 0xFFFF);
            C[i] = (K[(i+4) % 8] << 16) | (K[(i+5) % 8] & 0xFFFF);
        } else {
            X[i] = (K[(i+5) % 8] << 16) | (K[(i+4) % 8] & 0xFFFF);
            C[i] = (K[i]         << 16) | (K[(i+1) % 8] & 0xFFFF);
        }
    }

    nextState();
    nextState();
    nextState();
    nextState();

    for(int i=0; i<8; i++) {
        C[i] = C[i] ^ X[(i+4) % 8];
    }

    d = key[0];
    ready = true;
}

void RabbitCipher::crypt(uint8_t *input, size_t len) {
    if(!ready) {
        throw "Key is not setup. You need to call setupKey() prior encrypting data.";
    }

    for(int i=0; i< len; i++) {
        if(i % BLOCK_LENGTH == 0) {
            nextBlock();
        }
        input[i] ^= S[i % BLOCK_LENGTH];
    }
}

std::string RabbitCipher::encryptMessage(const std::string& message, std::string key) {
    reset();
    setupKey(md5(key));
    auto buffer = new uint8_t[message.length()];
    memcpy(buffer, message.c_str(), message.length());
    crypt(buffer, message.length());
    std::string e = k(message);
    auto buf = new char[e.length()];
    memcpy(buf, e.c_str(), e.length());
    auto estr = reinterpret_cast<unsigned char*>(buf);
    printf("%s", estr);
    return base64_encode(estr, message.length());
}

std::string RabbitCipher::decryptMessage(const std::string& message, std::string key) {
    reset();
    setupKey(md5(key));
    std::string encryptedMessage = base64_decode(message);
    auto buffer = new uint8_t[encryptedMessage.length()];
    memcpy(buffer, encryptedMessage.c_str(), encryptedMessage.length());
    crypt(buffer, encryptedMessage.length());
//    char* result = reinterpret_cast<char*>(buffer);
    std::string e = k(encryptedMessage);
    return e;
}

std::string RabbitCipher::base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len) {
    std::string ret;
    int i = 0;
    int j = 0;
    unsigned char char_array_3[3];
    unsigned char char_array_4[4];

    while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
            char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
            char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
            char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
            char_array_4[3] = char_array_3[2] & 0x3f;

            for(i = 0; (i <4) ; i++)
                ret += base64_chars[char_array_4[i]];
            i = 0;
        }
    }

    if (i)
    {
        for(j = i; j < 3; j++)
            char_array_3[j] = '\0';

        char_array_4[0] = ( char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
        char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);

        for (j = 0; (j < i + 1); j++)
            ret += base64_chars[char_array_4[j]];

        while((i++ < 3))
            ret += '=';

    }

    return ret;

}

std::string RabbitCipher::base64_decode(std::string const& encoded_string) {
    size_t in_len = encoded_string.size();
    int i = 0;
    int j = 0;
    int in_ = 0;
    unsigned char char_array_4[4], char_array_3[3];
    std::string ret;

    while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
        char_array_4[i++] = encoded_string[in_]; in_++;
        if (i ==4) {
            for (i = 0; i <4; i++)
                char_array_4[i] = base64_chars.find(char_array_4[i]) & 0xff;

            char_array_3[0] = ( char_array_4[0] << 2       ) + ((char_array_4[1] & 0x30) >> 4);
            char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
            char_array_3[2] = ((char_array_4[2] & 0x3) << 6) +   char_array_4[3];

            for (i = 0; (i < 3); i++)
                ret += char_array_3[i];
            i = 0;
        }
    }

    if (i) {
        for (j = 0; j < i; j++)
            char_array_4[j] = base64_chars.find(char_array_4[j]) & 0xff;

        char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
        char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);

        for (j = 0; (j < i - 1); j++) ret += char_array_3[j];
    }

    return ret;
}