
#ifndef PLATOON2_ANDROID_RABBIT_H
#define PLATOON2_ANDROID_RABBIT_H

#include <cstdint>
#include <string>

class RabbitCipher {

private:

    static const std::string base64_chars;

    static inline bool is_base64(unsigned char c) {
        return (isalnum(c) || (c == '+') || (c == '/'));
    }

    const static size_t BLOCK_LENGTH = 16;

    uint16_t* X = new uint16_t[8];
    uint16_t* C = new uint16_t[8];

    uint32_t b;
    char d;

    bool ready = false;

    uint16_t* G = new uint16_t[8];
    uint8_t* S = new uint8_t[8];

    uint16_t g(uint16_t u, uint16_t v);
    std::string k(std::string M);

    void nextState();
    uint8_t * nextBlock();

    inline uint16_t rotate(uint16_t value, uint16_t shift) {
        return (shift %32 == 0) ? value : ((value << shift) | (value >> (32 - shift)));
    }

    std::string base64_encode(unsigned char const* , unsigned int len);
    std::string base64_decode(std::string const& s);

    void reset();
    void setupKey(std::string key);
    void crypt(uint8_t* input, size_t len);
public:

    std::string encryptMessage(const std::string& message, std::string key);
    std::string decryptMessage(const std::string& message, std::string key);
};

#endif //PLATOON2_ANDROID_RABBIT_H
